import Foundation

extension Set where Element == String {
  func sortAndJoin() -> String {
    return self.sorted().joined()
  }
}