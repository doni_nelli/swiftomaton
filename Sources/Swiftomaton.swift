class Automaton {
  let q0: String
  let F: Set<String>
  let T: [String: [Character: Set<String>]]

  init(q0: String, F: Set<String>, T: [String: [Character: Set<String>]]) {
    self.q0 = q0
    self.F = F
    self.T = T
  }

  lazy private(set) var isDeterministic: Bool = {
    for transition in self.T.values {
      for destinies in transition.values {
        if destinies.count > 1 {
          return false
        }
      }
    }
    return false
  }()

  lazy private(set) var alphabet: Set<Character> = {
    var alphabet = Set<Character>()
    for transition in self.T.values {
      for a in transition.keys {
        alphabet.update(with: a)
      }
    }
    return alphabet
  }()

  func evaluate(word: String) -> Bool {
    var current = Set([q0])
    for a in word.characters {
      current = closure(from: exits(from: current, withSymbol: a))
    }
    return !F.intersection(current).isEmpty
  }

  func asDeterministic() -> Automaton {
    if isDeterministic {
      return self
    }

    var newTransitions = [String: [Character: Set<String>]]()
    var newFinals = Set<String>()
    var stateSets = Set<Set<String>>([[q0]])
    var queue = stateSets

    while let S = queue.popFirst() {
      newTransitions[S.sortAndJoin()] = [:]
      for a in alphabet {
        let ps = exits(from: S, withSymbol: a)
        if !stateSets.contains(ps) {
          stateSets.update(with: ps)
          queue.update(with: ps)
        }
        newTransitions[S.sortAndJoin()]![a] = [ps.sortAndJoin()]
        if !ps.intersection(F).isEmpty {
          newFinals.update(with: ps.sortAndJoin())
        }
      }
    }

    return Automaton(q0: q0, F: newFinals, T: newTransitions)
  }

  private func exits(from S: Set<String>, withSymbol a: Character) -> Set<String> {
    var result = Set<String>()
    for q in S {
      if let ps = T[q]?[a] {
        result.formUnion(ps)
      }
    }
    return result
  }

  private func closure(from S: Set<String>) -> Set<String> {
    var result = Set<String>(S)
    var queue = Array<String>(S)

    while let q = queue.popLast() {
      let ps = exits(from: [q], withSymbol: "$").symmetricDifference(result)
      result.formUnion(ps)
      queue.append(contentsOf: ps)
    }

    return result
  }
}
