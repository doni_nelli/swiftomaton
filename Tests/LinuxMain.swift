import XCTest
@testable import SwiftomatonTests

XCTMain([
    testCase(SwiftomatonTests.allTests),
])
